#include<stdio.h> //printf
#include<string.h>    //strlen
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#include<stdlib.h>
 
#define LITTLE_E 0
#define BIG_E 1

int endian(){
	int i = 1;
	char *p = (char*)&i;

	if(p[0] == 1)
		return BIG_E;
	else
		return LITTLE_E;
}

void convertInt(int *old){
	if(endian() == LITTLE_E)
		{
		int new = *old;
		char* old_ptr = (char*)old;
		char* new_ptr = (char*)&new;

		old_ptr[0] = new_ptr[3];
		old_ptr[1] = new_ptr[2];
		old_ptr[2] = new_ptr[1];
		old_ptr[3] = new_ptr[0];
	}
}

void convertTime(char str[])
{
	if(endian() == LITTLE_E)
	{
	    char c;
	    char *p, *q;
 
	    p = str;
	    if (!p)
	        return;
 
	    q = p + 1;
	    if (*q == '\0')
	        return;
	 
	    c = *p;
	    convertTime(q);
 
	    while (*q != '\0') {
	        *p = *q;
	        p++;
	        q++;
	    }
	    *p = c;
 
	    return;
	}
}

void convertFloat(float *old){
	if(endian() == LITTLE_E)
	{
		float new = *old;
		char* old_ptr = (char*)old;
		char* new_ptr = (char*)&new;

		old_ptr[0] = new_ptr[3];
		old_ptr[1] = new_ptr[2];
		old_ptr[2] = new_ptr[1];
		old_ptr[3] = new_ptr[0];
	}
}

int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;
    char message[2000] ;
    int type;
     
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Gniazdo utworzone");
     
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8878 );
 
    //Connect to remote server
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
     
    puts("Polaczono!");
     
    while(1)
    {	
	printf("*****************************\n\t1002 - godzina\n\t1003 - pierwiastki równania\n*****************************\n");
        printf("Podaj numer polecenia : ");
        scanf("%d", &type);
		convertInt(&type);
		if( send(sock , &type , sizeof(type) , 0) < 0)
        {
            puts("Send failed");
            return 1;
        }
         
        //Receive a reply from the server
		type = 0;
    	if( recv(sock , &type , sizeof(type) , 0) < 0)
    	{
    		puts("recv failed");
        	break;
    	}

		convertInt(&type);
		if(type == 3002)
		{
			puts("\nOdpowiedz serwera: :");
			printf("Typ: %d, serwer zrozumial polecenie.\n", type);
			int sizeOfBufor;
			if( recv(sock , &sizeOfBufor , sizeof(int) , 0) < 0)
        		{
            			puts("recv failed");
            			break;
        		}
			char* bufor;
			convertInt(&sizeOfBufor);
			bufor = malloc(sizeof(char)* sizeOfBufor);
			if( recv(sock , bufor , sizeof(char) * sizeOfBufor , 0) < 0)
        		{
            			puts("recv failed");
            			break;
        		}
			convertTime(bufor);
			puts(bufor);
			free(bufor);
		} 
		else if(type == 3003)
		{
			float a, b, c, x1, x2;
			int kod_rozwiazania;
			puts("Równanie kwadratowe ma postać Ax^2 + Bx + C");
			puts("Podaj A B C (po spacji):");
			scanf("%f %f %f", &a,&b,&c);
			convertFloat(&a);
			convertFloat(&b);
			convertFloat(&c);			

			if( send(sock , &a , sizeof(float) , 0) < 0
			|| send(sock , &b , sizeof(float) , 0) < 0
			|| send(sock , &c , sizeof(float) , 0) < 0)
        	{
            	puts("Send failed");
            	return 1;
        	}
			puts("\nOdpowiedz serwera: :");
			printf("Typ: %d, serwer zrozumial polecenie.\n", type);
			if(recv(sock , &kod_rozwiazania , sizeof(kod_rozwiazania) , 0) < 0)
        	{
            	puts("recv failed");
            	break;
        	}
			convertInt(&kod_rozwiazania);
			if(kod_rozwiazania == 0)
			{
				puts("Brak rozwiazan!");
			}
			if(kod_rozwiazania == 1)
			{
				if(recv(sock , &x1 , sizeof(x1) , 0) < 0)
        		{
            		puts("recv failed");
            		break;
        		}
				convertFloat(&x1);
				printf("Jedno rozwiazanie, x = %f\n",x1);
			}
			if(kod_rozwiazania == 2)
			{
				if( recv(sock , &x1 , sizeof(x1) , 0) < 0
				| recv(sock , &x2 , sizeof(x2) , 0) < 0)
        		{
            		puts("recv failed");
            		break;
        		}
				convertFloat(&x1);
				convertFloat(&x1);
				printf("Dwa rozwiazania, x1 = %f\tx2 = %f\n",x1, x2);
			}
			if(kod_rozwiazania == 3)
			{
				puts("To nie jest funkcja kwadratowa!");
			}
			
		} else 
		{
			puts("Server nie rozpoznal polecenia!");
		}
    }
     
    close(sock);
    return 0;
}