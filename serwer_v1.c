#include<stdio.h>
#include<string.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include <time.h>
#include <math.h>

#define SIZE_OF_TIME 9
#define LITTLE 0
#define BIG 1

int endian(){
	int i = 1;
	char *p = (char*)&i;

	if(p[0] == 1)
		return BIG;
	else
		return LITTLE;
}

void convertInt(int *old){
	if(endian() == LITTLE)
		{
		int new = *old;
		char* old_ptr = (char*)old;
		char* new_ptr = (char*)&new;

		old_ptr[0] = new_ptr[3];
		old_ptr[1] = new_ptr[2];
		old_ptr[2] = new_ptr[1];
		old_ptr[3] = new_ptr[0];
	}
}

void convertTime(char str[])
{
	if(endian() == LITTLE)
	{
	    char c;
	    char *p, *q;
 
	    p = str;
	    if (!p)
	        return;
 
	    q = p + 1;
	    if (*q == '\0')
	        return;
	 
	    c = *p;
	    convertTime(q);
 
	    while (*q != '\0') {
	        *p = *q;
	        p++;
	        q++;
	    }
	    *p = c;
 
	    return;
	}
}

void convertFloat(float *old){
	if(endian() == LITTLE)
	{
		float new = *old;
		char* old_ptr = (char*)old;
		char* new_ptr = (char*)&new;

		old_ptr[0] = new_ptr[3];
		old_ptr[1] = new_ptr[2];
		old_ptr[2] = new_ptr[1];
		old_ptr[3] = new_ptr[0];
	}
}
 
char* getTime(){
	time_t current_time;
	struct tm * time_info;
	static char timeString[SIZE_OF_TIME];

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(timeString, SIZE_OF_TIME, "%H:%M:%S", time_info);
	return timeString;
}

int main(int argc , char *argv[])
{
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
    const int backlog = 3;
    int type;
    
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8878 );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
     
    //Listen
    listen(socket_desc , backlog);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
     
    //accept connection from an incoming client
    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
    puts("Connection accepted");
     
    //Receive a message from client
    while( (read_size = recv(client_sock , &type , sizeof(type) , 0)) > 0 )
    {
	convertInt(&type);
	if(type == 1002){
		type = 3002;
		convertInt(&type);
	        write(client_sock , &type , sizeof(type));
		char* time;		
		time = getTime();
		int size = SIZE_OF_TIME;
		convertInt(&size);
        	write(client_sock , &size , sizeof(int));
		convertTime(time);
		write(client_sock , time , size);
	} 
	else if(type == 1003) 
	{
		type = 3003;
		convertInt(&type);
        	write(client_sock , &type , sizeof(type));
		float a, b, c, x1, x2;
		float delta;
		int kod_rozwiazania;
		if( (read_size = recv(client_sock , &a , sizeof(float) , 0)) > 0
		| (read_size = recv(client_sock , &b , sizeof(float) , 0)) > 0
		| (read_size = recv(client_sock , &c , sizeof(float) , 0)) > 0)
		{
			convertFloat(&a);
			convertFloat(&b);
			convertFloat(&c);
			
			printf("A=%f B=%f C=%f\n", a,b,c);
			if(a!=0)
			{
				delta = b*b-4*a*c;
				if(delta > 0)
				{
					kod_rozwiazania = 2;
					x1 = (-b - sqrt(delta)) / 2 * a;
					x2 = (-b + sqrt(delta)) / 2 * a;
					convertInt(&kod_rozwiazania);
					convertFloat(&x1);
					convertFloat(&x2);
					
					write(client_sock , &kod_rozwiazania , sizeof(kod_rozwiazania));
					write(client_sock , &x1 , sizeof(x1));
					write(client_sock , &x2 , sizeof(x2));
				} 
				else if (delta == 0)
				{
					kod_rozwiazania = 1;
					x1 = -b / 2 * a;
						convertInt(&kod_rozwiazania);
						convertFloat(&x1);
					write(client_sock , &kod_rozwiazania , sizeof(kod_rozwiazania));
					write(client_sock , &x1 , sizeof(x1));	
				}
				else
				{
					kod_rozwiazania = 0;
					convertInt(&kod_rozwiazania);
					write(client_sock , &kod_rozwiazania , sizeof(kod_rozwiazania));
					//brak rozwiazan
				}
			}
			else
			{
				kod_rozwiazania = 3;
				convertInt(&kod_rozwiazania);
				write(client_sock , &kod_rozwiazania , sizeof(kod_rozwiazania));
				//to nie jest rownanie kwadratowe
			}
		}	
	} else 
	{
		type = 0000;
		convertInt(&type);
		write(client_sock , &type , sizeof(type));
	}
		
    }
     
    if(read_size == 0)
    {
        puts("Client disconnected");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
     
    return 0;
}